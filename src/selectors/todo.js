// Adicionando um cache para o selector
import { createSelector } from "reselect";

// const getTasks = (state) => state.todoReducers.tasks;

const getTasks = createSelector(
  (state) => state.todoReducers.tasks,
  (tasks) => tasks
);

const getAge = createSelector(
  (state) => state.todoReducers.age,
  (age) => age
);

const selectors = {
  getTasks,
  getAge,
};

export { selectors };

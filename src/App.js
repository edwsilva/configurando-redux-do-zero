//import React, { useState, useEffect } from "react";
import React, { useState } from "react";
//import { store } from "./store";
//import { connect } from "react-redux"; // conecta a Store ao componente
import { useSelector, useDispatch } from "react-redux"; // substitui o connect e os mapsStates

import { actions } from "./actions/todo";
import { selectors } from "./selectors/todo";

//const App = ({ tasks, addTask }) => {
const App = () => {
  const [task, updateTask] = useState("");
  const [add, setAgeAdd] = useState(0);
  const [sub, setAgeSub] = useState(0);
  //const [tasks, updateTasks] = useState([]);
  const dispatch = useDispatch(); // Vai ter acesso ao dispatch
  //const tasks = useSelector(state => state); // Faz o papel do mapStateToProps - passo uma funçao que devolve o estado
  //const tasks = useSelector((state) => state.todoReducers.tasks); // Faz o papel do mapStateToProps - passo uma funçao que devolve o estado
  const tasks = useSelector(selectors.getTasks);
  //console.log(tasks);
  const age = useSelector(selectors.getAge);
  //console.log(age);
  //const isloading = useSelector(selectors.getIsloading);
  // useEffect(() => {
  //   store.subscribe(() => {
  //     updateTasks(store.getState());
  //   });
  // }, []);

  const handleInputChange = (event) => {
    updateTask(event.target.value);
  };
  const handleInputChangeIdadeMais = (event) => {
    setAgeAdd(event.target.value);
  };
  const handleInputChangeIdadeMenos = (event) => {
    setAgeSub(event.target.value);
  };
  const handleFormSubmit = (event) => {
    event.preventDefault();
    //console.log("TIPO DE ADD " + typeof add);
    // Pega o array atual, cria um novo array, adiciona a tarefa atual e con
    // catena com o array de tarefas (tasks)
    // updateTasks(oldTasks => [
    //   ...oldTasks,
    //   task,
    //   console.log(oldTasks)
    // ])

    // store.dispatch({
    //   type: "ADD_TASK",
    //   payload: task
    // });
    //addTask(task)

    // *** const dispatch = useDispatch() // Vai ter acesso ao dispatch - HOOK
    // dispatch({
    //   type: 'TODO_ADD_TASK',
    //   payload: task,
    // })
    dispatch(actions.addTask(task));
    dispatch(actions.ageUp(parseFloat(add)));
    dispatch(actions.ageDown(parseFloat(sub)));
    updateTask(""); //limpa o campo de digitação
  };

  return (
    <>
      <form onSubmit={handleFormSubmit}>
        <input onChange={handleInputChange} value={task} />
        <button>Add</button>
        <input onChange={handleInputChangeIdadeMais} value={add} />
        <button>Adicionar Idade</button>
        <input onChange={handleInputChangeIdadeMenos} value={sub} />
        <button>Subtrair Idade</button>
      </form>

      <ul>
        {tasks.map((t, i) => (
          <li key={i}>{t}</li>
        ))}
      </ul>
    </>
  );
};

// const mapStateToProps = state => ({ //Tenho acesso ao STATE
//   tasks: state
// }); // chegam como propriedade para o componente
// const mapDispatchToProps = dispatch => ({ //tenho acesso ao DISPATCH
//   addTask: task =>
//     dispatch({
//       type: "ADD_TASK",
//       payload: task
//     })
// });

//export default connect(mapStateToProps, mapDispatchToProps)(App);
export default App;

import { combineReducers } from "redux";
import { handleActions } from "redux-actions";
//import { types } from "../types/todo";
import { actions } from "../actions/todo";

// const tasks = (state = [], action) => {
//   console.log(action);
//   switch (action.type) {
//     case types.TODO_ADD_TASK:
//       return [...state, action.payload];

//     default:
//       return state;
//   }
// };
const tasks = handleActions(
  {
    //passa um objeto como primeiro parâmetro e um array como segundo.
    // No primeiro parâmetro (objeto) devemos mapear as nossas ações e o que fazer quando ela for chamada.
    // Utilizamos a própria ação como case / if
    // No handleActions passamos nossas funções com a ação a ser executada quando for chamada.
    [actions.addTask]: (state, action) => [...state, action.payload],
  },
  []
);

const age = handleActions(
  {
    [actions.ageUp]: (state, action) => ({
      age: (state.age += action.payload),
    }),

    [actions.ageDown]: (state, action) => ({
      age: (state.age -= action.payload),
    }),
  },
  
  { age: 0 }
);

const pageList = handleActions(
  {
    [actions.getPageList]: (state, action) => ({

      ...state,
      pageList: [],
    })
  },
  {pageList: []
  
  }
);

const reducers = combineReducers({
  tasks,
  age,
});

export { reducers };
